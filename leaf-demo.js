// See post: http://asmaloney.com/2014/01/code/creating-an-interactive-map-with-leaflet-and-openstreetmap/
var map = L.map('map', {
	center: [20.0, 5.0],
	minZoom: 2,
	zoom: 3
})

L.tileLayer('https://api.mapbox.com/styles/v1/develop-t-rocx-lificloud/cit33izst003m2ymqlj3ma27x/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoiZGV2ZWxvcC10LXJvY3gtbGlmaWNsb3VkIiwiYSI6ImNpdDM0dmNoMDB1OHMyemtobmMzc3F6aHQifQ.eSin4ZoOw0zpGE7uAe_miw', {
	//attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>',
	maxNativeZoom: 19,
	maxZoom: 25,
	attribution: "� Mapbox � OpenStreetMap � Oledcomm Data"
}).addTo(map)

var myURL = jQuery('script[src$="leaf-demo.js"]').attr('src').replace('leaf-demo.js', '')

//add click listener for images
$(document).mouseup(function (e) {
              if (!($('#profil')).is(e.target)){
		if(!$('#profil').hasClass('hidden'))
                    $('#profil').toggleClass('hidden');
              }
                });

var addIconImage = function(image){
var myIcon = L.icon({
	iconUrl: myURL + image,
	//iconRetinaUrl: myURL + 'pin48.png',
	iconSize: [24, 24],
	//iconSize: [40, 35],
	iconAnchor: [9, 21],
	popupAnchor: [0, -14]
});

return myIcon;
}

var addIconUser = function(image){
var myIcon = L.icon({
	iconUrl: myURL + image,
	//iconRetinaUrl: myURL + 'pin48.png',
	//iconSize: [24, 24],
	iconSize: [40, 35],
	iconAnchor: [9, 21],
	popupAnchor: [0, -14]
});

return myIcon;
}

var myLamp = L.icon({
	iconUrl: myURL + 'IconeLampe.png',
	//iconRetinaUrl: myURL + 'pin48.png',
	iconSize: [29, 24],
	//iconSize: [40, 35],
	iconAnchor: [9, 21],
	popupAnchor: [0, -14]
});


for (var i = 0; i < markersLampes.length; ++i) {
	L.marker([markersLampes[i].lat, markersLampes[i].long], {
			icon: myLamp
		})
		.addTo(map);
}

function onClick(e) {
	window.open(this.options.win_url);
}

map.on('zoomend', function (e) {
    zoom_based_layerchange();
});

var searchInput = $("#search-input");
var searchButton = $("#search-button");
var test=0;

searchButton.click(function(e){
    e.preventDefault();

    var addr = searchInput.val();
    searchAddress(addr);
    if(addr=="12 avenue de l'europe"||addr=="10 avenue de l'europe"){
		   if(test<1){
			//PLOT PICTOGRAMMES
			plotMarkersPictos();
			//PLOT USERS MARKERS
			plotAndroidDataMarkers();
			// ADD VELIZY'S INDOORMAP
			$.ajax({
		  	 dataType: "json",
    		   	url: "buildingContour.geojson",
    		   	success: function (data) {
        	   	var states = data;
		   	L.geoJson(states,{fillColor: '#D7D6D2',fillOpacity: 0,style:{color:'#F79F81',weight: 4}}).addTo(map);
    		   	}
			});

			$.ajax({
		  	 dataType: "json",
    		   	url: "building.geojson",
    		   	success: function (data) {
        	   	var states = data;
		   	L.geoJson(states,{style:{fillColor: '#D7D6D2',fillOpacity: 1,color:'#F79F81',weight: 2}}).addTo(map);
    		   	}
			});
		
			$.ajax({
		  	 dataType: "json",
    		   	url: "buildingCouloir.geojson",
    		   	success: function (data) {
        	   	var states = data;
		   	L.geoJson(states,{style:{fillColor: '#F2F2F2',fillOpacity: 1,color:'#F79F81',weight: 2}}).addTo(map);
    		   	}
			});

		     }
		test=1;
	        setInterval(function() {
    			getAndroidData(addr);
    		}, 500); // repeat interval: 500ms

    }
});

//search bar ajax request
var searchAddress = function(addressStr){
    $.ajax({
        method:"post",
        url:"https://maps.google.com/maps/api/geocode/json?address="+addressStr+"&sensor=false&language=fr&key=AIzaSyA6Fshd89xW7jYRov2C5Umc4kNuAVXfE_8",
        success:function(msg){
            console.log(msg);

            if(msg.status === "OK"){
                var obj = msg.results[0];

                //ZOOM ON ADDRESS
				map.setZoom(18);
				map.panTo([obj.geometry.location.lat, obj.geometry.location.lng]);



				//ADD MARKER ON THE MAP
                // setTimeout(function(){map.setZoom(15)}, 300);
        //         L.marker([obj.geometry.location.lat, obj.geometry.location.lng], {
        // 			icon: myIcon
        // 		}).bindPopup(obj.formatted_address)
        // 		  .addTo(map);

            }
        },
        failure:function(msg){
            console.log(msg);
        }

    });
}

//ajax query to db (transmitted data by android)
var usersName=['Aicha','Xu','Alex','Carlos'];
var mymarker0;
var mymarker1;
var mymarker2;
var mymarker3;
var myMarker;
var userCo="";
var k;
var data;
var mac;
var id;
var i;
var idUser;
var myMovingMarker;
var getAndroidData = function(addressA){
		i=0;
		//ADD & UPDATE USERS MARKERS POSITION
			// on lance une requ�te AJAX
			$.ajax({
				url : "ConnexionBase.php",
				type : 'GET',
				success : function(data){
					if (data) {
						console.log(data);
						while(i<data.length) {
							mac=data.substr(i+1,2);
							id=data.charAt(i)-1;
							console.log(id,mac);
							//map.removeLayer(markers[id]);
							userCo=usersName[id];
							console.log(userCo);
							k=0;
							while ((userCo!==markersUsers[k].name) && (k<markersUsers.length)) {
								k++
							}
							if(k<markersUsers.length){
								idUser=k;
							}
							console.log(markersUsers[idUser].name);
							k=0;
							while ((mac!==markersLampes[k].macAdress) && (k<markersLampes.length)) {
								k++
							}
							if(k < markersLampes.length && (markersUsers[idUser].lat !== markersLampes[k].lat || markersUsers[idUser].long !== markersLampes[k].long)){
								markersUsers[idUser].lat  = markersLampes[k].lat;
								markersUsers[idUser].long = markersLampes[k].long;
								window['myMarker'+idUser].slideTo( [markersUsers[idUser].lat, markersUsers[idUser].long-0.000007], {
    								duration: 1000,
    								keepAtCenter: false
								});
					
							}
							else if(k >= markersLampes.length)
								console.log('failure get mac address');
							else
								console.log('Not mouvement');

								
							//i=(id.length+mac.length)+1;
							i+=3;
						}

					}
					else
						console.log('failure get data from server');					

				}
			});
}

var plotAndroidDataMarkers = function(){
		icon0=addIconUser('Icone0.png');
		icon1=addIconUser('Icone1.png');
		icon2=addIconUser('Icone2.png');
		icon3=addIconUser('Icone3.png');

		myMarker0=L.marker([markersUsers[0].lat, markersUsers[0].long], {
			icon: icon0
		})
		.on('click', onClick0);
		function onClick0(myMarker0) {launchUserProCard('Aicha.png');}
		//myMarker0.addTo(map);

		myMarker1=L.marker([markersUsers[1].lat, markersUsers[1].long], {
			icon: icon3
		})
		.on('click', onClick1);
		function onClick1(e) {launchUserProCard('Xu.png');}
		myMarker1.addTo(map);

		myMarker2=L.marker([markersUsers[2].lat, markersUsers[2].long], {
			icon: icon2
		})
		.on('click', onClick2);
		function onClick2(e) {launchUserProCard('Alex.png');}
		//myMarker2.addTo(map);

		myMarker3=L.marker([markersUsers[3].lat, markersUsers[3].long], {
			icon: icon1
		})
		.on('click', onClick3);
		function onClick3(e) {launchUserProCard('Carlos.png');}
		//myMarker3.addTo(map);
}

var deletAndroidDataMarkers = function(){
	for (var i = 0; i < markersUsers.length; ++i) {
		map.removeLayer(markersUsers[i]);	}
}

//plot markers pictogrammes
var miicon0,miicon1,miicon2,miicon3,miicon4,miicon5,miicon6,miicon7,miicon8;
miicon0=addIconImage('IconeOffice.png');
miicon1=addIconImage('IconeRecep.png');
miicon2=addIconImage('IconeMeet.png');
miicon3=addIconImage('IconeIssue.png');
miicon4=addIconImage('IconeFood.png');
miicon5=addIconImage('IconeEscalier.png');
miicon6=addIconImage('IconeCoffe.png');
miicon7=addIconImage('IconeAscenseur.png');
miicon8=addIconImage('IconeWc.png');
miicon9=addIconImage('IconeTest.png');
miicon10=addIconImage('IconeStock.png');

var plotMarkersPictos = function(){
for(var u=0;u<markersPictos.length;u++){
 switch (u) {
  case 0:
	L.marker([markersPictos[0].lat, markersPictos[0].long], {
			icon: miicon3
		})
		.bindPopup(markersPictos[0].name)
		.addTo(map);
		break;
  case 7:
	L.marker([markersPictos[7].lat, markersPictos[7].long], {
			icon: miicon8
		})
		.bindPopup(markersPictos[7].name)
		.addTo(map);
		break;
  case 9:
	L.marker([markersPictos[9].lat, markersPictos[9].long], {
			icon: miicon6
		})
		.bindPopup(markersPictos[9].name)
		.addTo(map);
		break;
  case 10:
	L.marker([markersPictos[10].lat, markersPictos[10].long], {
			icon: miicon4
		})
		.bindPopup(markersPictos[10].name)
		.addTo(map);
		break;
  case 13:
	L.marker([markersPictos[13].lat, markersPictos[13].long], {
			icon: miicon2
		})
		.bindPopup(markersPictos[13].name)
		.addTo(map);
		break;
  case 17:
	L.marker([markersPictos[17].lat, markersPictos[17].long], {
			icon: miicon7
		})
		.bindPopup(markersPictos[17].name)
		.addTo(map);
		break;
  case 18:
	L.marker([markersPictos[18].lat, markersPictos[18].long], {
			icon: miicon5
		})
		.bindPopup(markersPictos[18].name)
		.addTo(map);
		break;
  case 19:
	L.marker([markersPictos[19].lat, markersPictos[19].long], {
			icon: miicon1
		})
		.bindPopup(markersPictos[19].name)
		.addTo(map);
		break;
  case 15:
	L.marker([markersPictos[15].lat, markersPictos[15].long], {
			icon: miicon10
		})
		.bindPopup(markersPictos[15].name)
		.addTo(map);
		break;
  case 3:
	L.marker([markersPictos[3].lat, markersPictos[3].long], {
			icon: miicon10
		})
		.bindPopup(markersPictos[3].name)
		.addTo(map);
		break;
  case 6:
	L.marker([markersPictos[6].lat, markersPictos[6].long], {
			icon: miicon9
		})
		.bindPopup(markersPictos[6].name)
		.addTo(map);
		break;
  case 8:
	L.marker([markersPictos[8].lat, markersPictos[8].long], {
			icon: miicon9
		})
		.bindPopup(markersPictos[8].name)
		.addTo(map);
		break;
  default:
	L.marker([markersPictos[u].lat, markersPictos[u].long], {
			icon: miicon0
		})
		.bindPopup(markersPictos[u].name)
		.addTo(map);
		break;
 }
}
}


var launchUserProCard = function(image){
	if($('#profil').hasClass('hidden'))
                    $('#profil').toggleClass('hidden')
	$('#profil').html('<img src="' + image + '" style="width:350px" >');
 
}

function zoom_based_layerchange() {

var currentZoom = map.getZoom();
if(currentZoom<20){
	if(!$('.leaflet-marker-pane').children().not("img[src$='IconeLampe.png']").hasClass('hidden'))
		$('.leaflet-marker-pane').children().not("img[src$='IconeLampe.png']").toggleClass('hidden');
}
else{
	if($('.leaflet-marker-pane').children().not("img[src$='IconeLampe.png']").hasClass('hidden'))
		$('.leaflet-marker-pane').children().not("img[src$='IconeLampe.png']").toggleClass('hidden');
}
}

